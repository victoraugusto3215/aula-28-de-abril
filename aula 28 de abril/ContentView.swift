//
//  ContentView.swift
//  aula 28 de abril
//
//  Created by COTEMIG on 28/04/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
