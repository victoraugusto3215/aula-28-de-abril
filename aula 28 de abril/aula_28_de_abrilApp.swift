//
//  aula_28_de_abrilApp.swift
//  aula 28 de abril
//
//  Created by COTEMIG on 28/04/22.
//

import SwiftUI

@main
struct aula_28_de_abrilApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
